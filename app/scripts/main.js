'use strict';

function toggleTabs() {
    const tabContainer = document.getElementById('tab-buttons');
    const tabContent = document.querySelector('.js-tabs-content');
    let tabButton = tabContainer.querySelectorAll('.js-tab-button');
    let tabContentItem = tabContent.querySelectorAll('.tabs__content');
    let i;

    for (i = 0; i < tabButton.length; i++) {
        tabButton[i].addEventListener('click', function () {
            let activeButton = this.getAttribute('data-tab');

            for (let j = 0; j < tabContentItem.length; j++) {
                tabContentItem[j].classList.remove('is-active');

                if (tabContentItem[j].getAttribute('data-tab') === activeButton) {
                    tabContentItem[j].classList.add('is-active');
                }
            }
        }, false);
    }
}

toggleTabs();

//scroll to
$('.js-link').click(function (e) {
  e.preventDefault();

  let scroll_el = $(this).attr('href');
  $('body, html').animate({
    scrollTop: $(scroll_el).offset().top + 10
  }, 600);

});

